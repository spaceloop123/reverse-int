const assert = require('assert');

const reverse = require('../src');

it('Should return 261 when 162 given', () => {
  const reversed = reverse(162);

  assert.equal(reversed, 261);
});
it('Should return 291 when -192 given', () => {
  const reversed = reverse(-192);

  assert.equal(reversed, 291);
});
it('Should return 252 when -252 given', () => {
  const reversed = reverse(-252);

  assert.equal(reversed, 252);
});
it('Should return 71 when 170 given', () => {
  const reversed = reverse(170);

  assert.equal(reversed, 71);
});
it('Should return 605 when 506 given', () => {
  const reversed = reverse(506);

  assert.equal(reversed, 605);
});
it('Should return 835 when -538 given', () => {
  const reversed = reverse(-538);

  assert.equal(reversed, 835);
});
